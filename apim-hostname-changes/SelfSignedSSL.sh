#create keystore
#keytool -genkey -alias dev.wso2.com -keyalg RSA -keysize 2048 -keystore devwso2.jks -validity 4000 -dname “CN=dev.wso2.com, OU=WSO2-Dev-API, O=WSO2-Dev, L=New Delhi, ST=Delhi, C=IN" -storepass wso2carbon -keypass wso2carbon
keytool -genkey -alias dev.wso2.com -keyalg RSA -keysize 2048 -keystore devwso2.jks -validity 4000

#export public key
keytool -export -alias dev.wso2.com -keystore devwso2.jks -file devwso2.pem

#export public key as base64
keytool -exportcert -alias dev.wso2.com -keystore devwso2.jks -rfc -file devwso2.pem

#import public key in client-truststore of wso2apim
keytool -import -alias dev.wso2.com -file devwso2.pem -keystore client-truststore.jks -storepass wso2carbon

#delete alias if exists
keytool -delete -alias dev.wso2.com  -keystore client-truststore.jks 
